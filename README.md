# Dinlang Mkdocs

## Getting started

Site web du projet ANR DINLANG basé sur le logiciel MkDocs pour construire un site statique.

## Utilisation
Installer la commande mkdocs (c'est une commande python)

Faire
```
mkdocs serve
```
pour tester localement sur http://127.0.0.1:8000/

Faire
``` 
mkdocs build
```
puis copier le répertoire "site" sur le serveur
