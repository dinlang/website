# Vie du projet

L’idée est de s'imprégner de la vie du chercheur en retraçant toutes les étapes effectuées pour le projet, du recrutement des familles à la communication orale. 
Pour cela, présence d’un schéma global illustratif cliquable qui mène aux différentes étapes du travail de recherche (recueil de données, en train de filmer, en train de brainstormer, en train d’analyser, en train de présenter).

<!--
![La vie du projet](img/vie-du-projet.png)
 -->

<img src="../img/vie-du-projet.png" usemap="#vdpmap" id="vdpmapid" alt="vie du projet" style="width: 100%;"/>
<map name='vdpmap'>
    <area target="" alt="w1" id="w1" href="w1" coords="1417,866,1017" shape="circle" alt='' href="#wp1" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w2" id="w2" href="w2" coords="3964,1041,998" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w21" id="w21" href="w21" coords="5820,991,387" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w22" id="w22" href="w22" coords="6837,1995,399" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w23" id="w23" href="w23" coords="5952,2792,438" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w24" id="w24" href="w24" coords="4246,2842,382" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w3" id="w3" href="w3" coords="2427,3074,969" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w31" id="w31" href="w31" coords="947,2917,349" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w32" id="w32" href="w32" coords="163,3689,352" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w33" id="w33" href="w33" coords="1380,4366,332" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w34" id="w34" href="w34" coords="2666,4454,362" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w4" id="w4" href="w4" coords="4510,4215,964" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w41" id="w41" href="w41" coords="5946,4316,358" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area target="" alt="w42" id="w42" href="w42" coords="6742,4325,360" shape="circle" alt='' href="#" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
</map>

<script>
    const vdpimage = document.querySelector('#vdpmapid')
    const w1Area = document.querySelector('#w1')
    const w2Area = document.querySelector('#w2')
    const w21Area = document.querySelector('#w21')

    function setCoordsAreaCircle(image, area, sx, sy) {
        const imgwidth = image.clientWidth
        const imgheight = image.clientHeight
        let arraycoords = area.getAttribute('coords').split(',')
        let myarray = []
        myarray.push((arraycoords[0] / sx) * imgwidth);
        myarray.push((arraycoords[1] / sy) * imgheight );
        ratio = ((sx / imgwidth) + (sy / imgheight)) / 2;
        myarray.push(arraycoords[2] * ratio);
        area.setAttribute('coords', myarray.join(','))
    }

    function setCoordsWP1() { setCoordsAreaCircle(vdpimage, w1Area, 7100, 5100); }
    function setCoordsWP2() { setCoordsAreaCircle(vdpimage, w2Area, 7100, 5100); }
    function setCoordsWP21() { setCoordsAreaCircle(vdpimage, w21Area, 7100, 5100); }
    setCoordsWP1();
    setCoordsWP2();
    setCoordsWP21();
    new ResizeObserver(setCoordsWP1).observe(vdpimage);
    new ResizeObserver(setCoordsWP2).observe(vdpimage);
    new ResizeObserver(setCoordsWP21).observe(vdpimage);
    
</script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/maphilight/1.4.0/jquery.maphilight.min.js"></script>
<script>$('#vdpmapid').maphilight();</script>

<style>
.wrapper {
  display: grid;
  grid-template-columns: 5% 95%;
}
</style>

<div class="wrapper">
  <div>WP1</div>
  <div>
<h2 id="w1">WP1 : GESTION DE PROJET</h2>
Ce premier Work Package, dédié au pilotage et à l’organisation du projet Dinlang, est animé par Aliyah Morgenstern et Sophie de Pontox. Pour le mener à bien, elles sont naturellement assistées de tous les porteurs du projet.

Nous assurons le suivi du projet Dinlang grâce à divers outils logiciels dont Excel. Pour que tous les membres de l’équipe soient régulièrement et facilement informés des dernières avancées, nous avons créé une liste de diffusion CNRS. C’est d’autant plus important que les quatre équipes impliquées œuvrent depuis des sites différents.

Nos documents partagés sont sur un drive de l’université Sorbonne Nouvelle auquel seuls les membres du projet ont accès.

C’est dans le cadre de ce Work Package que nous définissons nos bonnes pratiques, organisons nos réunions, décidons des missions, gérons l’agenda et le budget, échangeons sur la charge auteurs, dialoguons avec les experts extérieurs et, au final, prenons toutes les décisions relatives au projet Dinlang.
  </div>
  <div>WP2</div>
  <div>
  </div>
  <div>WP2.1</div>
  <div>
  </div>
  <div>WP2.2</div>
  <div>
  </div>
  <div>WP2.3</div>
  <div>
  </div>
  <div>WP2.4</div>
  <div>
  </div>
  <div>WP3</div>
  <div>
  </div>
  <div>WP3.1</div>
  <div>
  </div>
  <div>WP3.2</div>
  <div>
  </div>
  <div>WP3.3</div>
  <div>
  </div>
  <div>WP3.4</div>
  <div>
  </div>
  <div>WP4</div>
  <div>
  </div>
  <div>WP4.1</div>
  <div>
  </div>
  <div>WP4.2</div>
  <div>
  </div>
</div>
