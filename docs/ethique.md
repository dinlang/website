# ÉTHIQUE
## Questions éthiques
La conception et le protocole de collecte et d’analyse des données ont été approuvés par la commission recherche de l’université de la Sorbonne Nouvelle et sont en cours de présentation au Comité d’Ethique de la Recherche. Les participants doivent avoir pleinement conscience que l’équipe de recherche aura accès aux données et que, s’ils donnent leur consentement, celles-ci seront ultérieurement accessibles à la communauté scientifique au sens large. Nous suivons les principes de données FAIR (Findable, Accessible, Interoperable and Reusable, Wilkinson et al., 2016) pour la collecte et la gestion des données.

La communauté a élaboré un guide des bonnes pratiques (Baude et al. 2006) auquel les responsables du projet ont ajouté leurs propres recommandations pour les membres de leurs équipes. Toutes les procédures ont été vérifiées auprès des services juridiques de leurs institutions respectives et les porteurs du projet sont en contact permanent avec leurs conseils de recherche et les experts du CNRS. Les questions éthiques soulevées par la collecte de données et d’informations personnelles dans les entretiens et les questionnaires sont contrôlées par notre comité consultatif.

Les familles participent de façon volontaire et gratuite. Le projet leur est décrit en LSF ou en français parlé (selon leur préférence) et en français écrit. Leur sont précisés l’objectif et la durée de la recherche, le nom des chercheurs, les intérêts de la recherche, des informations sur la protection des données et les coordonnées des responsables du projet (par e-mail, SMS ou téléphone).
Ce projet ne comporte aucun risque pour les familles. Ces dernières peuvent, à tout moment, mettre fin à leur participation et demander la destruction des données les concernant. Tous les participants adultes signeront un formulaire de consentement éclairé, adapté en format LSF-vidéo pour les sourds signants.
Les chercheurs peuvent évaluer la motivation des familles lors d’entretiens préliminaires et surtout répondre à leurs questions et/ou préoccupations concernant leur potentielle participation. Quant aux enfants, ils reçoivent des explications sur le projet adaptées à leur âge et donnent leur consentement oral, signé et/ou écrit à leur participation selon leurs préférences linguistiques et leur âge.
Nous respecterons tous les droits des participants tels que présentés dans le RGPD européen. Les données sont conformes à la méthodologie conseillée par la Commission Nationale de l’Informatique et des Libertés (CNIL).

## Stockage et partage
Les informations relatives au nom, à l’adresse et à la santé des participants seront strictement confidentielles. Les données seront stockées sur des serveurs protégés et cryptés. Les responsables du projet veillent personnellement à ce qu’aucune violation de la confidentialité susceptible de causer une quelconque détresse aux adultes ou aux enfants participant au projet ne soit possible. Le consentement éclairé sera stocké séparément des données.

Par ailleurs, les vidéos et les transcriptions sont stockées sur le serveur sécurisé d’ORTOLANG. Les données seront partagées, d’abord entre les membres du projet, puis, avec les membres de la communauté scientifique (avec le consentement éclairé des familles). Des outils de gestion de la protection des données sont utilisés avec le soutien de la TGIR Huma-Num. L’analyse des données vidéo numériques passe par la constitution d’archives coordonnées avec des possibilités d’interrogation et de visualisation sur des serveurs cryptés.

## Éthique spécifique aux repas
Les responsables du projet considèrent les repas comme un lieu de rencontre pour les membres de la famille et le projet DinLang valorise leurs bénéfices (en matière) de lien social. Nous avons pleinement conscience que les modes de consommation alimentaire sont liés à de subtiles questions sociales, culturelles, physiques et psychologiques. Aussi, nous ne ferons aucun commentaire susceptible d’être perçu comme une intrusion voire une agression culturelle ou médicale.

## Téléchargement des autorisations
Autorisation adulte pour participer au projet DinLang
Autorisation parentale pour que votre enfant participe au projet DinLang
