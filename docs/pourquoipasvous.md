<div class="container">
<div class="columns is-mobile">
  <div class="column">
# Pourquoi pas vous ?

<!-- ![](img/repas-familial.png) -->

Des familles de chaque groupe d’âge et de chaque population seront filmées chez elles, à deux reprises avec trois caméras, de la préparation du dîner au nettoyage.

Notre objectif étant de recueillir et de coder des interactions réelles dans le cadre d’un dîner, l’équipement utilisé en ce sens et le dispositif défini doivent permettre de capter autant d’informations que possible sans parasiter les participants au dîner.
Comme le lieu du dîner est fixe, toujours autour d’une table, de forme variable, le matériel d’enregistrement l’est aussi. Concrètement, nous utilisons trois caméras et un enregistreur de son. Une première caméra à 360° est placée au-dessus de la table sur un pied à perche, sur lequel est fixé l’enregistreur de son.

Cette caméra 360° délivre déjà de précieuses informations sur ce qui se passe à la table mais ne permet pas de coder parfaitement la direction du regard ainsi que la posture des participants. Pour pallier cet écueil, nous complétons donc le dispositif par deux caméras classiques à grand angle postées de part et d’autre de la table. Chaque caméra fournit une vue frontale/latérale de la moitié des participants et une vue arrière de l’autre moitié. Les caméras, que nous avons dotées de microphones de meilleure qualité, captent également le son.

Nous obtenons ainsi trois enregistrements vidéo et quatre enregistrements audio différents. La vidéo 360° peut être convertie en deux vidéos 180° pour faciliter son utilisation avec ELAN (2021). Tous les fichiers vidéo et audio sont synchronisés avec un clap vidéo classique.

L’enregistrement est ensuite analysé du premier appel à la table et est arrêté lorsque tous les participants l’ont quittée.
  </div>
  <div class="column">
# En LSF

<video width="90%" controls>
  <source src="https://ct3.ortolang.fr/pages/info-lsf-dinlang/dinlang-lsf.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video> 
  </div>
</div>
