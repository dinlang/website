# Vie du projet

L’idée est de s'imprégner de la vie du chercheur en retraçant toutes les étapes effectuées pour le projet, du recrutement des familles à la communication orale. 
Pour cela, présence d’un schéma global illustratif cliquable qui mène aux différentes étapes du travail de recherche (recueil de données, en train de filmer, en train de brainstormer, en train d’analyser, en train de présenter).


<style>
.wrapperH {
  display: grid;
  grid-template-columns: 6% 88% 6%;
}
</style>

<div class="wrapperH">
  <div>
  </div>
  <div>
<img src="../img/vie-du-projet.png" usemap="#vdpmap" id="vdpmapid" alt="vie du projet" style="width: 100%;"/>
<map name='vdpmap'>
    <area alt="w1" id="w1" href="./#wp1" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w2" id="w2" href="./#wp2" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w21" id="w21" href="./#wp21" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w22" id="w22" href="./#wp22" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w23" id="w23" href="./#wp23" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w24" id="w24" href="./#wp24" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w3" id="w3" href="./#wp3" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w31" id="w31" href="./#wp31" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w32" id="w32" href="./#wp32" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w33" id="w33" href="./#wp33" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w34" id="w34" href="./#wp34" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w4" id="w4" href="./#wp4" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w41" id="w41" href="./#wp41" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
    <area alt="w42" id="w42" href="./#wp42" coords="1,1" shape="poly" data-maphilight='{"fillColor":"550055","fillOpacity":0.6,"strokeWidth":3}'>
</map>
  </div>
  <div>
  </div>
</div>

<!-- COMMENTAIRE HTML -->

<script>
    w1Coords = [1270,13,825,82,543,320,392,677,379,1010,505,1399,687,1612,900,1775,1232,1819,1515,1838,1929,1763,2180,1506,2374,979,2330,615,2067,289,1759,138,1396,25];
    w2Coords = [3942,63,3547,138,3227,345,3020,665,2938,1073,3014,1424,3202,1713,3490,1876,3785,1963,4143,1976,4500,1863,4782,1612,4927,1286,4958,947,4883,615,4688,345,4412,151];
    w21Coords = [5755,656,5629,719,5560,801,5491,939,5479,1058,5504,1196,5667,1315,5886,1334,6087,1215,6150,970,6118,807,5949,688];
    w22Coords = [6840,1669,6677,1731,6595,1888,6601,2076,6677,2214,6921,2239,7015,2208,7091,2014,7059,1800,6965,1706];
    w23Coords = [5905,2515,5780,2603,5711,2748,5711,2930,5799,3036,5999,3080,6175,3011,6231,2835,6238,2691,6125,2547]
    w24Coords = [4143,2541,4049,2672,4005,2823,3998,3042,4136,3149,4425,3143,4525,3030,4544,2817,4494,2660,4331,2547]
    w3Coords = [2399,2064,2079,2101,1897,2233,1766,2346,1659,2497,1471,2785,1427,3011,1408,3268,1508,3450,1722,3764,1954,3896,2180,4021,2587,4027,2964,3864,3196,3582,3340,3206,3371,2930,3246,2666,3051,2365,2876,2152,2587,2083]
    w31Coords = [931,2635,737,2685,699,2810,637,2948,693,3099,875,3187,1057,3180,1214,3024,1226,2798,1101,2647]
    w32Coords = [85,3413,16,3475,-3,3839,3,3971,198,3971,386,3902,480,3682,373,3500,254,3419]
    w33Coords = [1251,4103,1120,4241,1126,4397,1220,4579,1352,4617,1540,4554,1684,4347,1546,4165,1408,4084]
    w34Coords = [2568,4190,2380,4285,2374,4529,2430,4661,2606,4673,2844,4623,2945,4473,2901,4278,2744,4184]
    w4Coords = [4174,3206,3817,3450,3622,3732,3534,4184,3559,4441,3779,4786,4124,5018,4525,5056,5071,5037,5234,4849,5472,4485,5485,4159,5403,3695,5127,3444,4782,3180,4456,3162]
    w41Coords = [5886,4034,5679,4178,5692,4498,5905,4579,6125,4561,6225,4416,6250,4190,6068,4034]
    w42Coords = [6614,4040,6451,4165,6457,4517,6608,4604,6846,4554,6984,4423,6934,4121,6746,4021]

    const vdpimage = document.querySelector('#vdpmapid')
    const w1Area = document.querySelector('#w1')
    const w2Area = document.querySelector('#w2')
    const w21Area = document.querySelector('#w21')
    const w22Area = document.querySelector('#w22')
    const w23Area = document.querySelector('#w23')
    const w24Area = document.querySelector('#w24')
    const w3Area = document.querySelector('#w3')
    const w31Area = document.querySelector('#w31')
    const w32Area = document.querySelector('#w32')
    const w33Area = document.querySelector('#w33')
    const w34Area = document.querySelector('#w34')
    const w4Area = document.querySelector('#w4')
    const w41Area = document.querySelector('#w41')
    const w42Area = document.querySelector('#w42')

    function setCoordsArea(image, area, arraycoords, sx, sy) {
        const imgwidth = image.clientWidth
        const imgheight = image.clientHeight
        let myarray = []
        for (let i = 0; i < arraycoords.length; i+=2) {
            myarray.push((arraycoords[i] / sx) * imgwidth);
            myarray.push((arraycoords[i+1] / sy) * imgheight );
        }
        area.setAttribute('coords', myarray.join(','))
    }

    /* commentaire bloc
    */
    // commentaire jusque fin de ligne

    function setCoordsWP1() { setCoordsArea(vdpimage, w1Area, w1Coords, 7100, 5100); } // penser à mettre à jour la taille de l'image
    function setCoordsWP2() { setCoordsArea(vdpimage, w2Area, w2Coords, 7100, 5100); }
    function setCoordsWP21() { setCoordsArea(vdpimage, w21Area, w21Coords, 7100, 5100); }
    function setCoordsWP22() { setCoordsArea(vdpimage, w22Area, w22Coords, 7100, 5100); }
    function setCoordsWP23() { setCoordsArea(vdpimage, w23Area, w23Coords, 7100, 5100); }
    function setCoordsWP24() { setCoordsArea(vdpimage, w24Area, w24Coords, 7100, 5100); }
    function setCoordsWP3() { setCoordsArea(vdpimage, w3Area, w3Coords, 7100, 5100); }
    function setCoordsWP31() { setCoordsArea(vdpimage, w31Area, w31Coords, 7100, 5100); }
    function setCoordsWP32() { setCoordsArea(vdpimage, w32Area, w32Coords, 7100, 5100); }
    function setCoordsWP33() { setCoordsArea(vdpimage, w33Area, w33Coords, 7100, 5100); }
    function setCoordsWP34() { setCoordsArea(vdpimage, w34Area, w34Coords, 7100, 5100); }
    function setCoordsWP4() { setCoordsArea(vdpimage, w4Area, w4Coords, 7100, 5100); }
    function setCoordsWP41() { setCoordsArea(vdpimage, w41Area, w41Coords, 7100, 5100); }
    function setCoordsWP42() { setCoordsArea(vdpimage, w42Area, w42Coords, 7100, 5100); }
    
    setCoordsWP1();
    setCoordsWP2();
    setCoordsWP21();
    setCoordsWP22();
    setCoordsWP23();
    setCoordsWP24();
    setCoordsWP3();
    setCoordsWP31();
    setCoordsWP32();
    setCoordsWP33();
    setCoordsWP34();
    setCoordsWP4();
    setCoordsWP41();
    setCoordsWP42();

    new ResizeObserver(setCoordsWP1).observe(vdpimage);
    new ResizeObserver(setCoordsWP2).observe(vdpimage);
    new ResizeObserver(setCoordsWP21).observe(vdpimage);
    new ResizeObserver(setCoordsWP22).observe(vdpimage);
    new ResizeObserver(setCoordsWP23).observe(vdpimage);
    new ResizeObserver(setCoordsWP24).observe(vdpimage);
    new ResizeObserver(setCoordsWP3).observe(vdpimage);
    new ResizeObserver(setCoordsWP31).observe(vdpimage);
    new ResizeObserver(setCoordsWP32).observe(vdpimage);
    new ResizeObserver(setCoordsWP33).observe(vdpimage);
    new ResizeObserver(setCoordsWP34).observe(vdpimage);
    new ResizeObserver(setCoordsWP4).observe(vdpimage);
    new ResizeObserver(setCoordsWP41).observe(vdpimage);
    new ResizeObserver(setCoordsWP42).observe(vdpimage);
    
</script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/maphilight/1.4.0/jquery.maphilight.min.js"></script>
<script>$('#vdpmapid').maphilight();</script>

<style>
.wrapper {
  display: grid;
  grid-template-columns: 5% 95%;
}
.relative { position: relative; width: 100%; height: 100%; }
.leftimg { position: relative; width: 100%; z-index: 2; }
.filler { background: #9F134D; width:10%; height:100%; z-index: 1; top: 0px; left: 50%; position: absolute; }
</style>

<div class="wrapper">
<div>
  <div class="relative">
    <img src="../img/0_PenserLeProjet.png" id="wp1img" alt="wp1" class="leftimg" />
    <div class="filler"></div>
  </div>
</div>
<div>
<h2 id="wp1">WP1 : GESTION DE PROJET</h2>
Ce premier Work Package, dédié au pilotage et à l’organisation du projet Dinlang, est animé par Aliyah Morgenstern et Sophie de Pontox. Pour le mener à bien, elles sont naturellement assistées de tous les porteurs du projet.

Nous assurons le suivi du projet Dinlang grâce à divers outils logiciels dont Excel. Pour que tous les membres de l’équipe soient régulièrement et facilement informés des dernières avancées, nous avons créé une liste de diffusion CNRS. C’est d’autant plus important que les quatre équipes impliquées œuvrent depuis des sites différents.

Nos documents partagés sont sur un drive de l’université Sorbonne Nouvelle auquel seuls les membres du projet ont accès.

C’est dans le cadre de ce Work Package que nous définissons nos bonnes pratiques, organisons nos réunions, décidons des missions, gérons l’agenda et le budget, échangeons sur la charge auteurs, dialoguons avec les experts extérieurs et, au final, prenons toutes les décisions relatives au projet Dinlang.
</div>
<div>
  <div class="relative">
    <img src="../img/1_RecueilDonnees.png" id="wp2img" alt="wp2" class="leftimg" />
    <div class="filler" style="background: #C33017;"></div>
  </div>
</div>
<div>
<h2 id="wp2">WP 2 : THÉORISATION / MODÉLISATION</h2>
Depuis la moitié des années 70, les théoriciens partagent l’idée que les enfants apprennent le langage lors de communications dyadiques certes, mais aussi dans le cadre d’interactions multipartites (Ochs & Schieffelin, 1984). En plus d’être une forme d’action sociale, le langage est un mode d’expérience du monde (Duranti, 2011, 2015 ; Ochs, 2012). A ce titre, en France, le repas du soir se présente comme un excellent lieu d’étude du rôle des interactions multipartites – en l’occurrence, entre parents et enfants – sur le développement du langage, les pratiques alimentaires et la socialisation des enfants.

La socialisation langagière stipule que le langage est un outil majeur pour inculquer et transformer la compétence socioculturelle tout au long de la vie (Ochs, 1988 ; Ochs & Schieffelin, 1984 ; Schieffelin & Ochs, 1986). Il s’agit d’analyser comment et pourquoi les jeunes enfants apprennent les compétences communicationnelles nécessaires pour « habiter » leurs identités culturelles par le biais du langage dans des activités particulières.

Notre cadre théorique intégratif combine de fait socialisation langagière, grammaire cognitive, approches interactionnelles et multimodales du langage et approche anthropologique de l’alimentation. Nous utilisons le terme « languaging » pour faire écho à l’utilisation multimodale du langage (Linell, 2009) et l’élargissons pour inclure parole, signes, gestuelle et actions.

Les repas familiaux n’ont en effet jamais été étudiés de manière approfondie en langue des signes, et encore moins dans des environnements bimodaux ou dans des cultures différentes, avec des cadres participatifs qui se chevauchent, des voix et des ressources sémiotiques qui s’entremêlent, se coordonnent, s’ajustent tout au long du dîner. Le projet Dinlang nous offre ainsi l’opportunité de réviser, généraliser, élargir et appliquer notre cadre théorique à des données écologiques multipartites, multimodales et multilingues.

Coordinateurs : Camille Debras, Charlotte Danino et les responsables du projet
</div>
<div>
  <div class="relative">
    <img src="../img/1_RecueilDonnees-point.png" id="wp21img" alt="wp21" class="leftimg" />
    <div class="filler" style="background: #C33017;"></div>
  </div>
</div>
<div>
<h2 id="wp21"></h2>
WP2.1
</div>
<div>
  <div class="relative">
    <img src="../img/1_RecueilDonnees-point.png" id="wp22img" alt="wp22" class="leftimg" />
    <div class="filler" style="background: #C33017;"></div>
  </div>
</div>
<div>
<h2 id="wp22"></h2>
WP2.2
</div>
<div>
  <div class="relative">
    <img src="../img/1_RecueilDonnees-point.png" id="wp23img" alt="wp23" class="leftimg" />
    <div class="filler" style="background: #C33017;"></div>
  </div>
</div>
<div>
<h2 id="wp23"></h2>
WP2.3
</div>
<div>
  <div class="relative">
    <img src="../img/1_RecueilDonnees-point.png" id="wp24img" alt="wp24" class="leftimg" />
    <div class="filler" style="background: #C33017;"></div>
  </div>
</div>
<div>
<h2 id="wp24"></h2>
WP2.4
</div>
<div>
  <div class="relative">
    <img src="../img/2_Analyses.png" id="wp3img" alt="wp3" class="leftimg" />
    <div class="filler" style="background: #165B65;"></div>
  </div>
</div>
<div>
<h2 id="wp3">WP 3 : DONNÉES</h2>
Les archives numériques seront à la base de toutes nos analyses et de l’adaptation de notre approche théorique. Elles constitueront, pour la communauté scientifique, les premières données familiales en français et en LSF. A ce titre, ces archives auront une grande valeur patrimoniale. Le plan de collecte des données a été précisément conçu pour révéler la capacité unique humaine à utiliser des ressources multimodales dans des activités situées avec différents types de participants et à différentes échelles de temps, laissant ainsi présager de fructueuses comparaisons.

Méthode
Les méthodes ethnographiques préconisées sont inspirées du projet CELF (UCLA). Le Pr Tamar Kremer-Sadlik, ancien directeur de recherche du CELF, formera les équipes en charge au début du projet. Nous utiliserons l’interopérabilité des logiciels à notre disposition pour combiner annotations manuelles et semi-automatiques.

Coordinateurs
Christophe Parisse (Equipe 2, MoDyCo) est chargé de l’encadrement du WP 3 et veille au stockage des données sur ORTOLANG et à la compatibilité des logiciels. Les équipes 1 (française) et 3 (LSF) seront responsables de la collecte des données.
</div>
<div>
  <div class="relative">
    <img src="../img/2_Analyses-point.png" id="wp31img" alt="wp31" class="leftimg" />
    <div class="filler" style="background: #165B65;"></div>
  </div>
</div>
<div>
<h2 id="wp31">Action 3.1 : Collecte des données</h2>
Des familles de chaque groupe d’âge et de chaque population seront filmées chez elles, à deux reprises avec trois caméras, de la préparation du dîner au nettoyage. Les enfants seront recrutés dans deux groupes d’âge : (a) de 3 à 5 ans (années de maternelle) ; et (b) de 6 à 10 ans (années d’école primaire). Nous veillerons à équilibrer les effectifs entre garçons et filles. Chaque session sera complétée par un entretien avec les parents et les enfants, lorsque ce sera possible, et fera l’objet d’un rapport d’analyse.
</div>
<div>
  <div class="relative">
    <img src="../img/2_Analyses-point.png" id="wp32img" alt="wp32" class="leftimg" />
    <div class="filler" style="background: #165B65;"></div>
  </div>
</div>
<div>
<h2 id="wp32">Action 3.2 : Premier niveau d’annotation</h2>
Les données dans les familles utilisant le français ou la LSF seront codées de la même façon pour les valeurs suivantes : participation, utilisation du regard, thèmes, productions sonores, productions gestuelles. Les productions seront codées séparément selon qu’elles induisent du sens ou sont de simples actions.
Pour le français, la partie sens sera constituée d’une transcription orthographique de l’oral, tandis que pour la LSF, les données seront annotées par ID-glosses (Johnston, 2010). Toutes les références personnelles et temporelles, vocales ou signées (français ou LSF), seront annotées.
Le format des données de Dinlang est disponible ici et sera mis à jour au fur et à mesure de l’avancée du projet, qui lui-même pourra être ajustés en fonction des premiers résultats.
</div>
<div>
  <div class="relative">
    <img src="../img/2_Analyses-point.png" id="wp33img" alt="wp33" class="leftimg" />
    <div class="filler" style="background: #165B65;"></div>
  </div>
</div>
<div>
<h2 id="wp33">Action 3.3 : Annotation semi-automatique du geste et du signe</h2>
Nous développerons un système de notation systématique du geste et du signe s’appuyant sur une approche kinésiologique du geste (Boutet, 2010). Openpose et Openface seront utilisés pour aligner les données 3D avec le codage multimodal manuel, et automatiser les couplages / associations forme-fonction visuelle et dynamique. Cela nous permettra d’augmenter considérablement le nombre de gestes et de signes annotés et la validité de nos analyses statistiques.
</div>
<div>
  <div class="relative">
    <img src="../img/2_Analyses-point.png" id="wp34img" alt="wp34" class="leftimg" />
    <div class="filler" style="background: #165B65;"></div>
  </div>
</div>
<div>
<h2 id="wp34">Action 3.4 : Plate-forme multimodale</h2>
Grâce à l’interopérabilité des logiciels utilisés, nous enregistrerons les composantes multimodales des formes cibles et les coupleront avec leur fonction. Nous créerons un dispositif multimodal permettant d’aligner temporellement toutes les caractéristiques des données articulatoires avec les données gestuelles et d’analyser l'(a)synchronie de la prosodie vocale et du geste (Dodane et al., 2019).
</div>
<div>
  <div class="relative">
    <img src="../img/3_Diffusion.png" id="wp4img" alt="wp4" class="leftimg" />
    <div class="filler" style="background: #FDAF00;"></div>
  </div>
</div>
<div>
<h2 id="wp4">WP 4 : ANALYSES</h2>
Le Work Package Analyses se décompose en deux volets :
– coordination des activités et des ressources sémiotiques
– pratiques langagières en français et en langue des signes française.

Nous vous proposons également de découvrir quelques exemples d’études en cours.
</div>
<div>
  <div class="relative">
    <img src="../img/3_Diffusion-point.png" id="wp41img" alt="wp41" class="leftimg" />
    <div class="filler" style="background: #FDAF00;"></div>
  </div>
</div>
<div>
<h2 id="wp41"></h2>
WP4.1
</div>
<div>
  <div class="relative">
    <img src="../img/3_Diffusion-point.png" id="wp42img" alt="wp42" class="leftimg" />
    <div class="filler" style="background: #FDAF00;"></div>
  </div>
</div>
<div>
<h2 id="wp42"></h2>
WP4.2
</div>
<div>
  <div class="relative">
    <img src="../img/3_Diffusion-point.png" id="wp5img" alt="wp5" class="leftimg" />
    <div class="filler" style="background: #FDAF00;"></div>
  </div>
</div>
<div>
<h2 id="wp5">WP 5 : VALORISATION</h2>
Présenter, partager nos activités et les résultats du projet Dinlang font partie intégrante de nos missions. Cette diffusion se fera auprès de nos pairs, à travers notre participation à des conférences internationales et la publication d’articles dans des revues de haut niveau. Nous organiserons également des ateliers où nos données et rapports seront accessibles à tous les membres de l’équipe.

Mais la portée de notre projet dépasse amplement le cadre universitaire et nous avons aussi à cœur de partager nos résultats avec les familles y ayant participé. Plus largement, nous imaginons une diffusion auprès du grand public à travers des documentaires scientifiques, un accès libre à une sélection d’extraits de vidéos, des livres pour enfants, ou encore un site internet trilingue (en Français, LSF et Anglais).

Nous souhaitons enfin proposer des présentations bimodales (parlées et signées) à l’attention des parents et aussi des professionnels spécialisés dans la petite enfance. Le cas échéant, des interprètes en langue des signes pourront être recrutés pour faciliter les échanges.

Coordinatrice
Aliyah Morgenstern (Equipe 1, Sorbonne Nouvelle)
</div>
</div>
